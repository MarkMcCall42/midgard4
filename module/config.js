import { armorConfig } from "./armorConfig";
import { playerRaces } from "./racesConfig";
import { weaponConfig } from "./weaponConfig";

export const midgard4 = {
    ...armorConfig,
    ...weaponConfig,
    materialTypes: {
        0: "midgard4.materialType.fabric",
        2: "midgard4.materialType.leather",
        3: "midgard4.materialType.metal"
    },
    playerRaces: {
        ...playerRaces
    }
}