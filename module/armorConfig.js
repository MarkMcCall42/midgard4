export const armorConfig = {
    armorClasses: {
        OR: "midgard4.armorClass.or",
        TR: "midgard4.armorClass.tr",
        LR: "midgard4.armorClass.lr",
        KR: "midgard4.armorClass.kr",
        PR: "midgard4.armorClass.pr",
        VR: "midgard4.armorClass.vr",
        RR: "midgard4.armorClass.rr"
    },
    armorTypes: {
        body: "midgard4.armorType.body",
        helmet: "midgard4.armorType.helmet",
        bracers: "midgard4.armorType.bracers",
        greaves: "midgard4.armorType.greaves",
        neck: "midgard4.armorType.neck"
    },
    protectionLocations: {
        body: "midgard4.protectionLocation.body",
        arms: "midgard4.protectionLocation.arms",
        legs: "midgard4.protectionLocation.legs",
        head: "midgard4.protectionLocation.head",
        neck: "midgard4.protectionLocation.neck",
        face: "midgard4.protectionLocation.face"
    },
    armor: {
        body: { type: "midgard4.armorType.body", location: "body", optionalLocations: ["arms", "legs"] },
        helmet: { type: "midgard4.armorType.helmet", location: "head", optionalLocations: ["face"] },
        bracers: { type: "midgard4.armorType.bracers", location: "arms" },
        greaves: { type: "midgard4.armorType.greaves", location: "legs" },
        neck: { type: "midgard4.armorType.neck", location: "neck" }
    }
}