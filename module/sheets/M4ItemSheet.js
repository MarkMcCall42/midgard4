import { getOptions, extractDisplayData, reconvertDisplayData } from './config/ItemConfig.js';

export default class M4ItemSheet extends ItemSheet {

    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            classes: ['midgard4', 'sheet', 'item']
        });
    }

    constructor(item, options) {
        super(item, mergeObject(options, getOptions(item.data.type)));
    }

    get template() {
        return `systems/midgard4/templates/sheets/${this.item.data.type}-sheet.hbs`;
    }

    getData() {
        const data = super.getData();
        console.log('getData()', data);
        // Update the data for the sheet (add data only needed there (Item-config, locations for wearables, eg))
        return {...data, config: CONFIG.midgard4, display: extractDisplayData(this.item.data.type, data.data) };
    }

}
Hooks.on('preUpdateItem', function(item, change, options, userId) {
    // Re-Convert data from sheet, if nec.
    console.log('preUpdate(item)', item);
    console.log('preUpdate(change)', change);
    reconvertDisplayData(item.data.type, change, item.data.data);
    change.display = undefined;
    console.log('preUpdate(2)', change);
    return true;
});