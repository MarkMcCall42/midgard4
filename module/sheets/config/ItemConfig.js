import ArmorConfig from './ArmorConfig.js';
import WeaponConfig from './WeaponConfig.js';
import SkillConfig from './SkillConfig.js';

const configs = {
    armor: new ArmorConfig(),
    weapon: new WeaponConfig(),
    skill: new SkillConfig()
}

const getFromConfig = (type, func, ...args) => {
    const config = configs[type];
    if (config && Handlebars.Utils.isFunction(config[func])) {
        return config[func](...(args || []));
    } else if (!config) {
        console.log(`No config for type '${type}'!`);
    } else {
        console.log(`No ${func}-function in ${type} config!`);
    }
}

export const getOptions = (type) => {
    return getFromConfig(type, 'getOptions') || {};
}

export const extractDisplayData = (type, data) => {
    return getFromConfig(type, 'extractDisplayData', data);
}

export const reconvertDisplayData = (type, displayData, currentData) => {
    return getFromConfig(type, 'reconvertDisplayData', displayData, currentData);
}