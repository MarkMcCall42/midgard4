import { midgard4 as Midgard4Config } from '../../config.js'

export default class ArmorConfig {

    getOptionalLocationsData(configData, actual) {
        if (Array.isArray(configData)) {
            const data = {};
            configData.forEach(key => {
                data[key] = (actual.indexOf(key) >= 0)
            })
            return data;
        }
    }

    extractDisplayData(data) {
        console.log("ArmorConfig.extractDisplayData()", data);
        const config = Midgard4Config.armor[data.armorType || '']
        const armorTypes = {};
        const displayData = {
            armorTypes: armorTypes
        }
        Object.keys(Midgard4Config.armor).forEach(k => {
            armorTypes[k] = Midgard4Config.armor[k].type;
        });
        if (config) {
            displayData['mainLocation'] = Midgard4Config.protectionLocations[config.location];
            displayData['optionalLocations'] = this.getOptionalLocationsData(config.optionalLocations, data.protectionLocations || []);
            displayData['protection'] = (data.protection === undefined ? data.materialType : data.protection);
        } else {
            console.log(`No armor config '${data.armorType}'`);
        }
        console.log("ArmorConfig.extractDisplayData() -> ", displayData);
        return displayData;
    }

    reconvertDisplayData(change, currentItemData) {
        const changeData = change.data || {};
        const displayData = change.display;
        console.log("ArmorConfig.reconvertDisplayData()", changeData, displayData, currentItemData);
        if (changeData.armorType) {
            // ArmorType was changed, rebuild protected locations...
            changeData.protectionLocations = [Midgard4Config.armor[changeData.armorType].location];
        } else if (displayData) {
            changeData.protection = displayData.protection;
            const protectionLocations = [Midgard4Config.armor[currentItemData.armorType].location];
            if (displayData.optionalLocations) {
                Object.keys(displayData.optionalLocations).forEach(key => {
                    if (displayData.optionalLocations[key] === true) {
                        protectionLocations.push(key);
                    }
                });
            }
            changeData.protectionLocations = protectionLocations;
        }
        change.data = changeData;
    }
}