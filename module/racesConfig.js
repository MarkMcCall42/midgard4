export const playerRaces = {
    human: "midgard4.races.human",
    elf: "midgard4.races.elf",
    dwarf: "midgard4.races.dwarf",
    halfling: "midgard4.races.halfling"
}

export const otherRaces = {
    orc: "midgard4.races.orc",
    goblin: "midgard4.races.goblin",
    undead: "midgard4.races.undead"
}