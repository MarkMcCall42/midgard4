import { midgard4 } from "./module/config.js"
import M4ItemSheet from "./module/sheets/M4ItemSheet.js";


Hooks.once("init", function() {
    console.log("Initializing Midgard 4th Ed. System...");

    CONFIG.midgard4 = midgard4;
    //Items.unregisterSheet("core", ItemSheet);
    Items.registerSheet("midgard4", M4ItemSheet, { makeDefault: true });

    Handlebars.registerHelper('concat', function(first, second) {
        return first + second;
    });

    /*
    Handlebars.registerHelper('callFunction', function(object, funcName, ...args) {
        const func = object[funcName];
        if (Handlebars.Utils.isFunction(func)) {
            console.log(`Calling function ${funcName}(${args})...`)
            return func(args)
        } else {
            console.log(`No function ${funcName} found in `, object);
        }
    });


    
        Handlebars.registerHelper('createOptions', function(object, data, readOnlyProperty, editableProperty, resultProperty) {
            let rVal = [];
            if(data) {
                let readOnly = data[readOnlyProperty]
                let editable = data[editableProperty]
                if(readOnly) {
                    if(!Handlebars.Utils.isArrays(readOnly)) {
                        readOnly = [readOnly]
                    }
                    readOnly.forEach(v => {
                        let current = `<input type="checkbox" >`
                    })
                }
                if(editable) {
                    if(!Handlebars.Utils.isArrays(editable)) {
                        editable = [editable]
                    }

                }
            }

            return rVal
        });
    */
});